package id.sch.smktelkommlg.tugas02.xirpl318.aacexampleroom.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
/**
 * Created by SMK TELKOM on 15/03/2018.
 */
@Entity
data class Note
(
        @ColumnInfo
        @PrimaryKey(autoGenerate = true)
        var id : Int = 0,
        @ColumnInfo
        var judul : String = "",
        @ColumnInfo
        var isi : String = ""
){

}