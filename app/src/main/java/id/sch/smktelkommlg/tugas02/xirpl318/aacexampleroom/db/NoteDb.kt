package id.sch.smktelkommlg.tugas02.xirpl318.aacexampleroom.db

import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.Database
import android.content.Context
import id.sch.smktelkommlg.tugas02.xirpl318.aacexampleroom.dao.NoteDao
import id.sch.smktelkommlg.tugas02.xirpl318.aacexampleroom.entity.Note

/**
 * Created by SMK TELKOM on 15/03/2018.
 */

  @Database(entities = [Note::class], version = 1, exportSchema = false)
//@Database(entities = arrayOf(Note::class), version = 1, exportSchema = false)
abstract class AppDB : RoomDatabase()
{
    abstract fun noteDao() : NoteDao

    companion object {
        private var INSTANCE: AppDB? = null

        fun getDB(context: Context): AppDB?
        {
            if (INSTANCE == null)
            {
                synchronized(AppDB::class)
                {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                            AppDB::class.java, "note.db")
                            .build()
                }
            }
            return INSTANCE
        }
    }

    fun destroyInstance()
    {
        INSTANCE = null
    }
}
