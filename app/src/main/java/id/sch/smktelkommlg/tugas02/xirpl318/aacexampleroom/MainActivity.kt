package id.sch.smktelkommlg.tugas02.xirpl318.aacexampleroom

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import id.sch.smktelkommlg.tugas02.xirpl318.aacexampleroom.adapter.NoteAdapter
import id.sch.smktelkommlg.tugas02.xirpl318.aacexampleroom.db.AppDB
import id.sch.smktelkommlg.tugas02.xirpl318.aacexampleroom.entity.Note

import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var mAdapter: NoteAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        initRV()
        initFab()

    }

    private fun initFab() {
        fab.setOnClickListener { view ->
            AppDB.getDB(this)?.noteDao()?.insert(Note())
//            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                  .setAction("Action", null).show()
    }

    private fun initRV() {
        recyclerViewNote.layoutManager = LinearLayoutManager(this)
        mAdapter = NoteAdapter()
        recyclerViewNote.adapter = mAdapter
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
