package id.sch.smktelkommlg.tugas02.xirpl318.aacexampleroom.repository

import id.sch.smktelkommlg.tugas02.xirpl318.aacexampleroom.dao.NoteDao
import id.sch.smktelkommlg.tugas02.xirpl318.aacexampleroom.db.AppDB
import id.sch.smktelkommlg.tugas02.xirpl318.aacexampleroom.entity.Note

/**
 * Created by SMK TELKOM on 15/03/2018.
 */
class NoteRepository(app: Application)
{
    private lateinit var mDao: NoteDao
    init {
        val db = AppDB.getDB(app)
        db?.let{mDao = it.noteDao()
        mItems = mDao.getAll()
        }
    }

    fun insert(item: Note)
    {
        insertAsyncTask(mDao).execute(item)
    }

    private class insert AsyncTask internal constructor(private val mTaskDao: NoteDao) :
        AsyncTask<Note, Void, Void>()
    {
        override fun doInBackground(vararg params: Note): Void?
        {
            mTaskDao.insert(params[0])
            return null
        }
    }
}