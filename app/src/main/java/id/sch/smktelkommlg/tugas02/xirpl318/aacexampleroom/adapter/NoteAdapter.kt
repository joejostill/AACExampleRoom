package id.sch.smktelkommlg.tugas02.xirpl318.aacexampleroom.adapter

import android.view.View
import android.view.ViewGroup
import id.sch.smktelkommlg.tugas02.xirpl318.aacexampleroom.entity.Note

/**
 * Created by SMK TELKOM on 15/03/2018.
 */
class NoteAdapter : RecyclerView.Adapter<NoteAdapter>.ViewHolder{

    private var items: List<Note>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VIewHolder
        = ViewHolder(parent.inflate(R.layout.item_note))

    override fun getItemCount() : Int = items?.size ?: 0
    override fun onBindViewHolder(holder: ViewHolder, position: Int)
        = holder.bind(items?.get(position) ?: Note())

    fun setNotes(notes : List<Note>){
        items = notes
    }

    class ViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView){
        fun bind(item: Note)
        {
            itemView.textViewJudul.text = item.judul
            itemView.textViewIsi.text = item.isi
        }
    }
}

fun Viewgroup.inflate(@LayoutREs layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutINfalter.from(context).infalte(layoutRes, this, attachToRoot)
}