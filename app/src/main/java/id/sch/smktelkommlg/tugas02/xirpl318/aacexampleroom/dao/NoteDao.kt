package id.sch.smktelkommlg.tugas02.xirpl318.aacexampleroom.dao

import id.sch.smktelkommlg.tugas02.xirpl318.aacexampleroom.entity.Note

/**
 * Created by SMK TELKOM on 15/03/2018.
 */
@Dao
interface NoteDao {
    @Query("SELECT * FROM Note")
    fun getAll() : List <Note>

    @Insert
    fun insert(note: Note)

    @Delete
    fun delete(note: Note)

    @Update
    fun update(note: Note)
}